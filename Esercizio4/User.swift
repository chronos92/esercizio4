//
//  User.swift
//  Esercizio4
//
//  Created by Manuel Ceschi on 11/07/18.
//  Copyright © 2018 Manuel Ceschi. All rights reserved.
//

import Foundation

class User {
    
    var id: Int
    var nome: String
    var cognome: String
    var telefono: String
    
    init (id: Int, nome: String, cognome: String, telefono: String){
        self.id = id
        self.nome = nome
        self.cognome = cognome
        self.telefono = telefono
    }
}

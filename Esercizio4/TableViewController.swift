//
//  TableViewController.swift
//  Esercizio4
//
//  Created by Manuel Ceschi on 12/07/18.
//  Copyright © 2018 Manuel Ceschi. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var array_user: [User] = []  //array di oggetti di tipo User
    var elemento_array: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // In attesa di una notifica di nome "nuovo utente creato"
        NotificationCenter.default.addObserver(self, selector: #selector(nuovo_utente(_:)), name: NSNotification.Name(rawValue: "nuovo utente creato"), object: nil) //nuovo utente è il nome della func che richiama
        NotificationCenter.default.addObserver(self, selector: #selector(utente_modificato(_:)), name: NSNotification.Name(rawValue: "utente modificato"), object: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //elimina il notification center dopo l'ascolto
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func nuovo_utente (_ notifica: Notification){
        
        let nuovo_utente = notifica.object as! User
        
        array_user.append(nuovo_utente)  //inserisce nuovo_utente in coda all'array
        
        array_user.sort { (User1, User2) -> Bool in  //ordinamento valori per nome
            return User1.nome < User2.nome
        }
        
        // ricarica la table view
        tableView.reloadData()
    }
    
    @objc func utente_modificato (_ notifica: Notification){
        
        let utente_modificato = notifica.object as! User
        
        let indice = array_user.index { (var_user) -> Bool in
            return var_user.id == utente_modificato.id
        }
        
        if indice != nil
        {
         array_user[indice!] = utente_modificato
            
         tableView.reloadData()
        }
        
    }
    
    
    // Numero di Sezioni = 1
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    // Numero di righe = numero elementi array_user
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_user.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "id_cella", for: indexPath)
        
        // salva in user l'ultima cella di array_user
        let user = self.array_user[indexPath.row]

        // configura titolo e sottotitolo della cella
        cell.textLabel?.text = user.nome+" "+user.cognome
        cell.detailTextLabel?.text = user.telefono

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let controller = segue.destination as! ViewController2
        
        
        if (segue.identifier == "modifica") {
            
            elemento_array = tableView.indexPathForSelectedRow!.row
            
            //passo l'oggetto alla view2
            controller.dati_lable_in = array_user[elemento_array]
        }
    }
    


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  Esercizio4
//
//  Created by Manuel Ceschi on 11/07/18.
//  Copyright © 2018 Manuel Ceschi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var array_user: [User] = []  //array di oggetti di tipo User

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(nuovo_utente(_:)), name: NSNotification.Name(rawValue: "nuovo utente creato"), object: nil) //nuovo utente è il nome della func che richiama
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //elimina il notification center dopo l'ascolto
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func nuovo_utente (_ notifica: Notification){
    
        let nuovo_utente = notifica.object as! User
        
        array_user.append(nuovo_utente)  //inserisce nuovo_utente in coda all'array
        array_user.sort { (User1, User2) -> Bool in  //ordinamento valori per nome
            return User1.nome > User2.nome
            
              
        }
    }
    


}

